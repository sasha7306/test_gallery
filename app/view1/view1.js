'use strict';

angular.module('myApp.view1', ['ngRoute', 'imageService'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view1', {
    templateUrl: 'view1/view1.html',
    controller: 'View1Ctrl'
  });
}])
.directive('checkImage', function($http) {
  return {
    link: function(scope, element, attrs) {
      element.bind('error', function() {
        element.attr('src', 'https://s.yimg.com/pw/images/en-us/photo_unavailable.png');
      });
    }
  };
})
.controller('View1Ctrl', ['$scope', 'ImageService', function ($scope, ImageService) {
  $scope.limit = "10";
  $scope.sort = 'title';
  $scope.search = '';
  $scope.pages = 0;
  $scope.currentPage = 0;
  $scope.selectedImage = 0;
  $scope.auto = true;

  ImageService.restoreState().then(function () {
    ImageService.deleteImage(83);

    if(!$scope.$$phase) {
      $scope.$apply(function(){
        getData();
        countPages();
      });
    } else {
      getData();
      countPages();
    }
  });

  var getData = function() {
    $scope.imageData = ImageService.getImages(Number($scope.limit),$scope.currentPage * Number($scope.limit), $scope.sort, $scope.search);
  };

  var countPages = function() {
    $scope.pages = Math.ceil($scope.imageData.count / Number($scope.limit));
    $scope.currentPage = 0;
  };

  $scope.updateData = function () {
    getData();
  };

  $scope.$watch("limit", function(newValue, oldValue) {
    if($scope.imageData)
      countPages();
  });

  $scope.$watch("search", function(newValue, oldValue) {
    if($scope.imageData)
      countPages();
  });

  $scope.changePage = function (newPage) {
    $scope.currentPage = newPage;
    getData();
  };

  $scope.nextPage = function () {
    if( $scope.currentPage + 1 <  $scope.pages ) {
      $scope.currentPage++;
      getData();
    }
  };

  $scope.prevPage = function () {
    if( $scope.currentPage > 0 ) {
      $scope.currentPage--;
      getData();
    }
  };

  $scope.selectImage = function (id) {
    $scope.selectedImage = id;
    $('.carousel').carousel(id);
  };

  $scope.nextImage = function () {
    $('.carousel').carousel('next');
    //$scope.selectedImage = ($scope.selectedImage + 1)%$scope.imageData.images.length;
  };

  $scope.prevImage = function () {
    $('.carousel').carousel('prev');
 /*   var imgCount = $scope.imageData.images.length;
    $scope.selectedImage = (imgCount + $scope.selectedImage - 1)%imgCount;*/
  };

  $scope.deleteImage = function (id) {
    ImageService.deleteImage(id);
    getData();
    countPages();
  };

  $scope.onChangeRotation = function () {
    if($scope.auto) {
      $('.carousel').carousel({
        interval: 2000
      });
    } else {
      $('.carousel').carousel('pause');
    }
  };
}]);
