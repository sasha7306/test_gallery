'use strict';

angular.module('imageService', [])
  .factory('ImageService', function($http) {
    var images;

    var saveState = function () {
      sessionStorage.images = angular.toJson(images);
    };

    var restoreState = function () {
      images = angular.fromJson(sessionStorage.images);
      if(!images) {
        return $http.get('model/images.json').then(function(response) {
          images = response.data.map(function (x, index) {
            x.id = index;
            return x;
          });
        });
      }else {
        return Promise.resolve();
      }
    };

    var deleteImage = function (id) {
      var index = images.findIndex(function (x) {
        return x.id === id;
      });
      if(index!==-1) {
        images.splice(index, 1);
        saveState();
      }
    };

    var getImages = function (limit, offset, sort, search) {
      var filteredImages = images.slice(0);
      if(search&&search.length>0) {
        filteredImages = filteredImages.filter(function (x) {
          return x.title.search(search)!==-1;
        });
      }

      switch (sort) {
        case 'title':
          filteredImages.sort(function(a, b) {
            if (a[sort] > b[sort]) return 1;
            if (a[sort] < b[sort]) return -1;
            return 0;
          });
          break;
        case 'date':
          filteredImages.sort(function(a, b) {
            return new Date(a.date) - new Date(b.date);
          });
          break;
      }

      return { images: filteredImages.slice(offset, offset + limit), count: filteredImages.length };
    };

    return {
      restoreState: restoreState,
      getImages: getImages,
      deleteImage: deleteImage
    };
  });


